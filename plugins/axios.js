import Vue from 'vue'

export default function ({ $axios, redirect }) {
  $axios.onRequest(config => {
    console.log('Making request to: ' + config.url)
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    var message = error.response.data.hasOwnProperty('message') 
                  ? resp.data.message 
                  : 'Please try again later.'
    if (code == 401 && Vue.$route.name == 'login') {
      return;
    }
    Vue.$swal(`Status code: ${code}`, message, 'error');
    return;
  })
}
