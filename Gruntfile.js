module.exports = function(grunt) {
  // Globbing load tasks
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    serverPort: 3000,
    stubbyPort: 8882,
    stubbyPath: "server/config.yaml",
    stubby: {
      stubsServer: {
        // note the array collection instead of an object
        options: {
          callback: function (server, options) {
            server.get(1, function (err, endpoint) {
              if (!err)
                console.log(endpoint);
            });
          },
          stubs: 8882,
          tls: 8444,
          admin: 8010
        },
        // note the array collection instead of an object
        files: [{
          src: ["server/*.yaml"]
        }]
      }
    },
    shell: {
      runDev: {
        command: "nuxt --open"
      },
      runStubby: {
        command: "stubby -d <%= stubbyPath %> -w <%= stubbyPath %>"
      }
    },
    open: { 
      server: {
        path: "http://localhost:<%= serverPort %>"
      }
    },
    watch: {
      options: {
        nospawn: true,
        livereload: true
      }
    }
  });

  grunt.registerTask('default', 'shell:runDev');
  grunt.registerTask('dev', ['shell:runDev']);
  grunt.registerTask('stubby', ['shell:runStubby']);
  grunt.registerTask('mock', function () {
    this.async();
    ['stubby', 'dev'].forEach(function (task) {
      grunt.util.spawn({grunt: true, args: [task]});
    });
  });
};