import webpack from 'webpack'
import APP_CONSTANTS from './constants'

export default {
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'static',
  /*
  ** Headers of the page
  */
  head: {
    title: 'LB-Portal Demo',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' }
    ],
    script: [
      { src: 'https://code.jquery.com/jquery-3.5.1.slim.min.js' },
      { src: 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js' },
      { src: 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js' }, 
      { src: 'https://cdn.jsdelivr.net/npm/showdown@1.9.1/dist/showdown.min.js' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { 
    color: '#28a745',
    height: '3px'
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/styles/main.css',
    '~/assets/sass/_first.sass',
    '~/assets/sass/_second.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/lodash',
    '~/plugins/axios',
    '~/plugins/mixins',
    '~/plugins/vue-swal',
    '~/plugins/vuelidate',
    '~/plugins/common.js'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    // 'nuxt-oauth'
  ],

  buildModules: [
    '@nuxtjs/dotenv'
  ],

  auth: {
    redirect: {
      login: '/login', // redirect user when not connected
      logout: '/',
      callback: '/auth/signed-in',
      home: '/'
    },
    //auth options
    strategies: {
      local: false,
      auth0: {
        domain: process.env.AUTH0_DOMAIN,
        client_id: process.env.AUTH0_CLIENT_ID
      }
    }
  },

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env.NODE_ENV == 'production' 
             ? 'https://my-json-server.typicode.com/trunglecntt/portal-api/' 
             : 'https://my-json-server.typicode.com/trunglecntt/portal-api/'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ]
  }, 
  
  env: {
    baseUrl: process.env.BASE_URL,
    appConsts: APP_CONSTANTS
  },

  router: {
    prefetchPayloads: false,
    middleware: ['auth']
  }
}
