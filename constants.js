// Import all children constants
import constants_1 from './constants/constants_1'

const APP_CONSTANTS = {
  apiBasePath: '/api',
  pagination: {
    limit: 2
  },
  constants1: constants_1
}
Object.freeze(APP_CONSTANTS)
export default APP_CONSTANTS